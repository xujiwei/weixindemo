package com.xjw.entity;

public class ViewButton extends Button {
	
	public ViewButton(){
		
	}
	
	public ViewButton(String name, String type, String url){
		this.name = name;
		this.type = type;
		this.url = url;
	}
	
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
