package com.xjw.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

public class AccessTokenUtil {
	
	private static String accessToken;
	
	private static long time = 0;
	
	public static String getAccessToken(){
		
		Date date = new Date();
		long timeNow = date.getTime();
		if (timeNow - time > 7000 * 1000) {
			accessToken = getNewAccessToken();
		}else{
			System.out.println("获取旧token:"+accessToken);
		}
		return accessToken;
	}
	
	public static String getNewAccessToken(){
		String accessToken = "";
		JSONObject jsonObject = HttpUtil.httpGet(SystemSetting.URL_ACCESS_TOKEN);
		accessToken = jsonObject.getString("access_token");
		System.out.println("获取新token:"+accessToken);
		return accessToken;
	}

}
