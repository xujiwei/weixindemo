package com.xjw.process;



import java.util.Date;

import com.xjw.util.MessageType;
import com.xjw.util.SystemSetting;
/**
 * 封装最终的xml格式结果
 * @author pamchen-1
 *
 */
public class FormatXmlProcess {
	/**
	 * 封装文字类的返回消息
	 * @param to
	 * @param from
	 * @param content
	 * @return
	 */
	public String formatTextXmlAnswer(String to, String from, String content) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[");
		sb.append(to);
		sb.append("]]></ToUserName><FromUserName><![CDATA[");
		sb.append(from);
		sb.append("]]></FromUserName><CreateTime>");
		sb.append(date.getTime());
		sb.append("</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[");
		sb.append(content);
		sb.append("]]></Content><FuncFlag>0</FuncFlag></xml>");
		return sb.toString();
	}
	
	/*
	 * 
	 */
	public String formatNewsXmlAnswer(String to, String from, String content) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[");
		sb.append(to);
		sb.append("]]></ToUserName><FromUserName><![CDATA[");
		sb.append(from);
		sb.append("]]></FromUserName><CreateTime>");
		sb.append(date.getTime());
		sb.append("</CreateTime><MsgType><![CDATA[news]]></MsgType><ArticleCount>1</ArticleCount><Articles>");
		sb.append("<item>").append("<Title><![CDATA[测试图文消息]]></Title>")
			.append("<Description><![CDATA[测试图文消息内容]]></Description>")
			.append("<PicUrl><![CDATA["+SystemSetting.BUSS_API_SUF+"/image/img.jpg]]></PicUrl>")
			.append("<Url><![CDATA[http://mp.weixin.qq.com/s?__biz=MzAwNzU4NTgyMQ==&mid=2456632889&idx=1&sn=786be57b3017964bb174e02ee0c5be84#rd]]></Url>")
			.append("</item>");
		sb.append("</Articles></xml>");
		return sb.toString();
	}
	
	/**
	 * 回复图片消息
	 * @param to
	 * @param from
	 * @param content
	 * @return
	 */
	public String formatImageXmlAnswer(String to, String from, String mediaId) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[")
		  .append(to)
		  .append("]]></ToUserName><FromUserName><![CDATA[")
		  .append(from)
		  .append("]]></FromUserName><CreateTime>")
		  .append(date.getTime())
		  .append("</CreateTime><MsgType><![CDATA[")
		  .append(MessageType.MESSAGE_IMAGE)
		  .append("]]></MsgType><Image><MediaId><![CDATA[")
		  .append(mediaId)
		  .append("]]></MediaId></Image></xml>");
		return sb.toString();
	}
	
	/**
	 * 发送语音消息
	 * @param to
	 * @param from
	 * @param mediaId
	 * @return
	 */
	public String formatVoiceXmlAnswer(String to, String from, String mediaId) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[")
		.append(to)
		.append("]]></ToUserName><FromUserName><![CDATA[")
		.append(from)
		.append("]]></FromUserName><CreateTime>")
		.append(date.getTime())
		.append("</CreateTime><MsgType><![CDATA[")
		.append(MessageType.MESSAGE_VOICE)
		.append("]]></MsgType><Voice><MediaId><![CDATA[")
		.append(mediaId)
		.append("]]></MediaId></Voice></xml>");
		return sb.toString();
	}
	/**
	 * 发送视频消息
	 * @param to
	 * @param from
	 * @param mediaId
	 * @return
	 */
	public String formatVideoXmlAnswer(String to, String from, String mediaId) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[")
		.append(to)
		.append("]]></ToUserName><FromUserName><![CDATA[")
		.append(from)
		.append("]]></FromUserName><CreateTime>")
		.append(date.getTime())
		.append("</CreateTime><MsgType><![CDATA[")
		.append(MessageType.MESSAGE_VIDEO)
		.append("]]></MsgType><Video><MediaId><![CDATA[")
		.append(mediaId)
		.append("]]></MediaId><Title><![CDATA[")
		.append("此处是标题")
		.append("]]></Title><Description><![CDATA[")
		.append("此处是描述")
		.append("]]></Description></Video></xml>");
		return sb.toString();
	}
	/**
	 * 发送音乐消息
	 * @param to
	 * @param from
	 * @param mediaId
	 * @return
	 */
	public String formatAudioXmlAnswer(String to, String from, String title, String mediaId) {
		StringBuffer sb = new StringBuffer();
		Date date = new Date();
		sb.append("<xml><ToUserName><![CDATA[")
		.append(to)
		.append("]]></ToUserName><FromUserName><![CDATA[")
		.append(from)
		.append("]]></FromUserName><CreateTime>")
		.append(date.getTime())
		.append("</CreateTime><MsgType><![CDATA[")
		.append(MessageType.MESSAGE_MUSIC)
		.append("]]></MsgType>")
		.append("<Music>")
		.append("<Title><![CDATA[")
		.append(title)
		.append("]]></Title><Description><![CDATA[")
		.append("此处是描述")
		.append("]]></Description>")
		.append("<MusicUrl><![CDATA[")
		.append(SystemSetting.BUSS_API_SUF+"audio/gangqinqu5.mp3")
		.append("]]></MusicUrl><HQMusicUrl><![CDATA[")
		.append(SystemSetting.BUSS_API_SUF+"audio/gangqinqu5.mp3")
		.append("]]></HQMusicUrl>")
		.append("</Music></xml>");
		return sb.toString();
	}
}
