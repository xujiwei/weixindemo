package com.xjw.entity;

public class Button {
	
	protected String name;
	protected String type;
	private Button[] sub_button;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Button[] getSub_button() {
		return sub_button;
	}
	public void setSub_button(Button[] subButton) {
		sub_button = subButton;
	}
}
