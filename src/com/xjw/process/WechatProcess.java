package com.xjw.process;

import com.xjw.entity.ReceiveXmlEntity;
import com.xjw.util.MessageType;
import com.xjw.util.SystemSetting;

/**
 * 微信xml消息处理流程逻辑类
 * @author pamchen-1
 *
 */
public class WechatProcess {
	/**
	 * 解析处理xml、获取智能回复结果（通过图灵机器人api接口）
	 * @param xml 接收到的微信数据
	 * @return	最终的解析结果（xml格式数据）
	 */
	public String processWechatMag(String xml){
		FormatXmlProcess formatXmlProcess =  new FormatXmlProcess();
		/** 解析xml数据 */
		ReceiveXmlEntity xmlEntity = new ReceiveXmlProcess().getMsgEntity(xml);
		
		
		String result = "";

		//事件消息
		if(MessageType.MESSAGE_EVENT.equals(xmlEntity.getMsgType())){
			if (MessageType.MESSAGE_SUBSCRIBE.equals(xmlEntity.getEvent())) {
				StringBuilder sb = new StringBuilder();
				sb.append("欢迎关注公众账号！\n")
				  .append("1、图文消息测试\n")
				  .append("2、文本消息测试\n")
				  .append("3、音乐消息测试\n")
				  .append("\"?\"调出菜单\n")
				  .append("<a href=\""+SystemSetting.BUSS_API_SUF+"/index.html\">进入主页</a>");
				result = sb.toString();
				result = formatXmlProcess.formatTextXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), result);
			}else if (MessageType.MESSAGE_CLICK.equals(xmlEntity.getEvent())) {
				if ("11".equals(xmlEntity.getEventKey())) {
					StringBuilder sb = new StringBuilder();
					//如果已经绑定用户，显示绑定信息
					if (SystemSetting.bindMap.containsKey(xmlEntity.getFromUserName())) {
						sb.append("您已绑定尾号为"+SystemSetting.bindMap.get(xmlEntity.getFromUserName())+"的卡片，您正在享受免费通知提醒功能。\n\n")
						  .append("<a href=\"http://xujiwei.tunnel.mobi/wenxinTest/WebChatSertlet?action=unbind&bindUser=")
						  .append(xmlEntity.getFromUserName())
						  .append("\">点击这里，解除绑定</a>");
					}else{//未绑定用户，则提示进行绑定
						sb.append("您还未绑定一卡通，绑定后即可享受免费通知提醒功能：\n\n")
						  .append("<a href=\"http://xujiwei.tunnel.mobi/wenxinTest/WebChatSertlet?action=bindFrom&bindUser=")
						  .append(xmlEntity.getFromUserName())
						  //.append("<a href=\"http://xujiwei.tunnel.mobi/wenxinTest/index.html")
						  .append("\">点击这里，立即绑定</a>");
						//SystemSetting.bindMap.put(xmlEntity.getFromUserName(), "5858");
					}
					result = sb.toString();
					result = formatXmlProcess.formatTextXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), result);
				}
			}
		}else{
			//接收的文本消息
			if (MessageType.MESSAGE_TEXT.equals(xmlEntity.getMsgType())) {
				result = xmlEntity.getContent();
				if ("1".equals(result)) {
					result = formatXmlProcess.formatNewsXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), result);
				}else if("2".equals(result)){
					result = formatXmlProcess.formatTextXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), "我就是文字");
				}else if("3".equals(result)){
					result = formatXmlProcess.formatAudioXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), "夜的钢琴曲5","");
				}else if("?".equals(result) || "？".equals(result)){
					StringBuilder sb = new StringBuilder();
					sb.append("欢迎关注测试公众账号！\n")
					  .append("1、图文消息测试\n")
					  .append("2、文本消息测试\n")
					  .append("3、音乐消息测试\n")
					  .append("\"?\"调出菜单\n")
					  .append("<a href=\""+SystemSetting.BUSS_API_SUF+"/index.html\">进入主页</a>");
					result = sb.toString();
					result = formatXmlProcess.formatTextXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), result);
				}else{
					result = formatXmlProcess.formatTextXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), result);
				}
			}else if (MessageType.MESSAGE_IMAGE.equals(xmlEntity.getMsgType())) {
				//接收到的图片链接
				result = xmlEntity.getPicUrl();
				String mediaId = xmlEntity.getMediaId();
				result = formatXmlProcess.formatImageXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), mediaId);
				System.out.println(result);
			}else if (MessageType.MESSAGE_VOICE.equals(xmlEntity.getMsgType())) {
//				String recognition = xmlEntity.getRecognition();
//				if ("".equals(recognition) || recognition == null) {
//					recognition = "未能识别语音结果！";
//				}
				String mediaId = xmlEntity.getMediaId();
				result = formatXmlProcess.formatVoiceXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), mediaId);
				System.out.println(result);
			}else if (MessageType.MESSAGE_SHORT_VIDEO.equals(xmlEntity.getMsgType())) {
				String mediaId = xmlEntity.getMediaId();
				result = formatXmlProcess.formatVideoXmlAnswer(xmlEntity.getFromUserName(), xmlEntity.getToUserName(), mediaId);
				System.out.println(result);
			}
		}
		return result;
	}
}
