package com.xjw.message.handle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xjw.process.WechatProcess;

public class TextMessageServlet extends HttpServlet {

	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		ServletOutputStream out = response.getOutputStream();
		String result = "dsddsadas";
		try {
			/** 读取接收到的xml消息 */
			StringBuffer sb = new StringBuffer();
			InputStream is = request.getInputStream();
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String s = "";
			while ((s = br.readLine()) != null) {
				sb.append(s);
			}
			String xml = sb.toString();	//次即为接收到微信端发送过来的xml数据
	
			/** 判断是否是微信接入激活验证，只有首次接入验证时才会收到echostr参数，此时需要把它直接返回 */
			String echostr = request.getParameter("echostr");
			if (echostr != null && echostr.length() > 1) {
				result = echostr;
			} else {
				//正常的微信处理流程
				result = new WechatProcess().processWechatMag(xml);
			}
			out.write(result.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			out.write(result.getBytes("UTF-8"));
			out.flush();
			out.close();
		}
	}

}
