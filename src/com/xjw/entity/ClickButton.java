package com.xjw.entity;

public class ClickButton extends Button {
	
	public ClickButton(){
		
	}
	
	public ClickButton(String name, String type, String key){
		this.name = name;
		this.type = type;
		this.key = key;
	}
	
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
