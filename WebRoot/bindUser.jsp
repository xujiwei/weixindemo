<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html">
<html>
<head>
<meta charset="UTF-8">
<title>关联一卡通</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery.mobile-1.4.3.js"></script>
<link rel="stylesheet" href="css/jquery.mobile-1.4.3.css" type="text/css"></link>
</head>
<body>
<div data-role="page" id="index">
	<div data-role="header">
		<h1>招商银行</h1>
	</div>
	<div role="main" class="ui-content">
	
	<form id="query" action="WebChatSertlet?action=bind">
		<div class="ui-field-contain">
		<label>微信用户：</label><input type="text" value="<%=session.getAttribute("nickname")%>" readonly="readonly"/>
		</div>
		<div class="ui-field-contain">
		<label>卡号：</label><input type="text" name="cardId"/>
		</div>
		<div class="ui-field-contain">
		<label>查询密码：</label><input type="password" name="cardPwd"/>
		</div>
		<button id="submit">提交</button>	
	</form>	
	</div>

</div>
</body>
</html>