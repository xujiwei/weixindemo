package com.xjw.util;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

public class SystemSetting {
	private static String ACCESS_TOKEN = "";
	public final static String APPID = "wx7b721892bef6abef";
	public final static String APPSECRET = "f3c968e69ad61501b16a9ef5dd3bdf47";
	//public final static String APPID = "wx8cdec7511464d97e";
	//public final static String APPSECRET = "98e41042a45612b6dd949fec8535971d";
	
	public final static String wxUserName = "xjwzxx2016";
	
	public final static String BUSS_API_SUF = "http://xujiwei.tunnel.qydev.com/wenxinTest/";
	
	public final static String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+APPID+"&secret="+APPSECRET;
	public final static String URL_GET_MENU = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";
	public final static String URL_DEL_MENU = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=";
	public final static String URL_CREATE_MENU = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";
	public final static String URL_GET_USER = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
	
	public final static Map<String, String> bindMap = new HashMap<String, String>();
	
	public static String getToken(){
		String token = "";
		if ("".equals(ACCESS_TOKEN) || false) {
			JSONObject jsonObject = HttpUtil.httpGet(URL_ACCESS_TOKEN);
			token = jsonObject.getString("access_token");
			if ("".equals(token) || token == null) {
				token = jsonObject.getString("errcode");
			}else{
				ACCESS_TOKEN = token;
			}
		}else{
			token = ACCESS_TOKEN;
		}
		return token;
	}
}

