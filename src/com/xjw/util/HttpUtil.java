package com.xjw.util;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpUtil {
	
	public static JSONObject httpGet(String url){
		JSONObject object = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse response = null;
		HttpEntity entity = null;
		try {
			response = httpclient.execute(httpGet);
			entity = response.getEntity();
			String result = EntityUtils.toString(entity,"utf-8");
			object = JSONObject.fromObject(result);
			return object;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				EntityUtils.consume(entity);
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return object;
	}
	
	public static JSONObject httpPost(String url,Map<String, String> params){
		JSONObject object = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		
		Set<String> keys = params.keySet();
		String value = "";
		for (String key : keys) {
			value = params.get(key);
			nvps.add(new BasicNameValuePair(key, value));
		}
		CloseableHttpResponse response = null;
		HttpEntity entity = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			response = httpclient.execute(httpPost);
			System.out.println(response.getStatusLine());
			entity = response.getEntity();
			String result = EntityUtils.toString(entity,"utf-8");
			object = JSONObject.fromObject(result);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			 try {
				EntityUtils.consume(entity);
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return object;
	}
	
	
	public static JSONObject httpPostStr(String url,String paramStr) throws ClientProtocolException, IOException{
		JSONObject object = null;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new StringEntity(paramStr,"utf-8"));
		HttpResponse httpResponse = httpClient.execute(httpPost);
		System.out.println(httpResponse.getStatusLine());
		HttpEntity entity = httpResponse.getEntity();
		String result = EntityUtils.toString(entity,"utf-8");
		object = JSONObject.fromObject(result);
		return object;
	}
	

}
