package com.xjw.manage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.xjw.entity.Button;
import com.xjw.entity.ClickButton;
import com.xjw.entity.Menu;
import com.xjw.entity.ViewButton;
import com.xjw.util.HttpUtil;
import com.xjw.util.SystemSetting;

public class MenuManage {


	@Test
	public void getMenu(){
		//String token = SystemSetting.getToken();
		String token = "xLNHo76njcPtynyryNmxqrA_qFTUlUBcHZVTayQPCO8o6GJtNHCmFckU2DblfVQqibrp-RO6KpDS68X894QqzqlEchJFhqqtfuUrehG9-uI";
		System.out.println(token);
		JSONObject object = HttpUtil.httpGet(SystemSetting.URL_GET_MENU + token);
		System.out.println(object.toString());
	}
	
	@Test
	public void delMenu(){
		String token = SystemSetting.getToken();
		System.out.println(token);
		JSONObject object = HttpUtil.httpGet(SystemSetting.URL_DEL_MENU + token);
		System.out.println(object.toString());
	}
	
	@Test
	public void createMenu() throws ClientProtocolException, IOException{
		//String token = SystemSetting.getToken();
		String token = "dNH_gXJsO1QDdfHBwb79e5fsmQc8tLZoLbsV5B0-aTTLXuGYH2kA2sOwvd4x32s4nab6nfCCdtzViS1EX99YcUG0JJzfdOecB3C2bqHVyUGIOWtfPvvJUw-A1jYtDD2OJEIaAGADYC";
		System.out.println(token);
		Button button1 = new Button();
		button1.setName("我");
		ClickButton button11 = new ClickButton("免费通知提醒","click","11");
		ViewButton button12 = new ViewButton("一卡通余额查询","view","http://xujiwei.tunnel.qydev.com/wenxinTest/WebChatSertlet");
		ClickButton button13 = new ClickButton("信用卡账单查询","click","13");
		ClickButton button14 = new ClickButton("我要办卡/贷款","click","14");
		ClickButton button15 = new ClickButton("我要购汇/结汇","click","15");
		button1.setSub_button(new Button[]{button11,button12,button13,button14,button15});
		
		Button button2 = new Button();
		button2.setName("发现");
		ViewButton button21 = new ViewButton("朝朝盈","view","http://xujiwei.tunnel.qydev.com/wenxinTest/WebChatSertlet");
		ViewButton button22 = new ViewButton("为小招点赞","view","http://xujiwei.tunnel.qydev.com/wenxinTest/WebChatSertlet");
		ClickButton button23 = new ClickButton("本地特惠","click","23");
		ClickButton button24 = new ClickButton("周边网店","click","24");
		button2.setSub_button(new Button[]{button21,button22,button23,button24});
		ClickButton button3 = new ClickButton();
		button3.setName("无卡取款");
		button3.setType("click");
		button3.setKey("3");
		
		Menu menu = new Menu();
		menu.setButton(new Button[]{button1,button2,button3});
		String jsonMenu = JSONObject.fromObject(menu).toString();
		System.out.println(jsonMenu);
		//Map<String, String> params = new HashMap<String, String>();
		//jsonMenu = "{\"button\":[{\"name\":\"11\",\"sub_button\":[{\"type\":\"click\",\"name\":\"11\",\"key\":\"V1001_GOOD\"}]}]}";
		//params.put("body", new String(jsonMenu.getBytes("gbk"),"utf-8"));
		JSONObject object = HttpUtil.httpPostStr(SystemSetting.URL_CREATE_MENU+token, jsonMenu);
		System.out.println(object.toString());
	}
}
